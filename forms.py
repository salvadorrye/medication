from django import forms
from .models import Medication
from bootstrap_modal_forms.forms import BSModalModelForm

class DateInput(forms.DateInput):
    input_type = 'date'

class MedicationCreateForm(BSModalModelForm):
    class Meta:
        model = Medication
        widgets = { 'date_ordered': DateInput(), }
        exclude = ('discontinued',)
