from django.db import models
from geriatrics.models import Admission
from django.conf import settings

SHIFT = (
        ('0', '6am-6pm'),
        ('1', '6pm-6am'),
        ('2', '6am-2pm'),
        ('3', '2pm-10pm'),
        ('4', '10pm-6am'),
        )

# Create your models here.
class Drug(models.Model):
    generic_name = models.CharField(max_length=200, db_index=True)
    brand_name = models.CharField(max_length=200, blank=True)
    dosage = models.CharField(max_length=70, blank=True)

    class Meta:
        ordering = ['generic_name']
        verbose_name = 'Medication'
        verbose_name_plural = 'Medications'

    def __str__(self):
        if self.brand_name and self.dosage:
            return f'{self.generic_name} ({self.brand_name}) {self.dosage}'
        elif self.brand_name:
            return f'{self.generic_name} ({self.brand_name})'
        elif self.dosage:
            return f'{self.generic_name} {self.dosage}'
        else:
            return f'{self.generic_name}'

class Medication(models.Model):
    created = models.DateField(auto_now_add=True, editable=False, null=False, blank=False)
    last_modified = models.DateField(auto_now=True, editable=False, null=False, blank=False)
    date_ordered = models.DateField(auto_now=False)
    admission = models.ForeignKey(Admission, on_delete=models.CASCADE, limit_choices_to={'discharged': False}) 
    drug = models.ForeignKey(Drug, on_delete=models.CASCADE, verbose_name='Medication', null=True, blank=False)
    frequency = models.CharField(max_length=70, blank=True)
    route = models.CharField(max_length=70, blank=True)
    discontinued = models.BooleanField(default=False)

    class Meta:
        ordering = ['-date_ordered']
        verbose_name = 'Medication order'
        verbose_name_plural = 'Medication orders'

    def __str__(self):
        return u'{}: {}'.format(self.admission.resident, self.drug)

class TimeToBeGiven(models.Model):
    medication = models.ForeignKey(Medication,
                              related_name='order',
                              on_delete=models.CASCADE)
    time_given = models.TimeField(auto_now=False, null=False, blank=False)

    class Meta:
        ordering = ['medication']
        verbose_name = 'Time given'
        verbose_name_plural = 'Times given'

    def __str__(self):
        return f'{self.medication} - {self.time_given.strftime("%-I:%M %p")}'

class MedicationSheet(models.Model):
    date = models.DateField(auto_now=False)
    shift = models.CharField(max_length=1, choices=SHIFT, blank=True) 
    nurse = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return f'{self.date} {self.get_shift_display()}-{self.nurse}'

class MedicationGiven(models.Model):
    medication_sheet = models.ForeignKey(MedicationSheet, on_delete=models.CASCADE)
    medication = models.ForeignKey(Medication, on_delete=models.CASCADE)
    time_given = models.TimeField(auto_now=False, null=False, blank=False)

    class Meta:
        verbose_name_plural = 'Medications given'

    def __str__(self):
        return f'"{self.medication}" Given: {self.time_given}'


