from django.urls import path
from . import views

app_name = 'medication'

urlpatterns = [
    path('meds/<int:pk>', views.MedicationsList.as_view(), name='meds-list'),
    #path('meds/create/', views.MedicationCreate.as_view(), name='meds-create'),
    path('meds/create/<int:pk>', views.MedicationCreate.as_view(), name='meds-create'),
    path('meds/<int:pk>/update', views.MedicationUpdate.as_view(), name='meds-update'),
    path('meds/<int:pk>/delete', views.MedicationDelete.as_view(), name='meds-delete'),
]
