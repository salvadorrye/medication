# Generated by Django 3.1.6 on 2021-12-29 01:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('medication', '0018_auto_20210829_1618'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='druginstance',
            name='brand',
        ),
        migrations.RemoveField(
            model_name='druginstance',
            name='drug',
        ),
        migrations.AlterModelOptions(
            name='drug',
            options={'ordering': ('generic_name',), 'verbose_name': 'Medication', 'verbose_name_plural': 'Medications'},
        ),
        migrations.AlterModelOptions(
            name='medication',
            options={'ordering': ('-date_ordered',), 'verbose_name': 'Medication order', 'verbose_name_plural': 'Medication orders'},
        ),
        migrations.RenameField(
            model_name='drug',
            old_name='name',
            new_name='generic_name',
        ),
        migrations.RemoveField(
            model_name='drug',
            name='category',
        ),
        migrations.RemoveField(
            model_name='drug',
            name='indication',
        ),
        migrations.RemoveField(
            model_name='drug',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='medication',
            name='available',
        ),
        migrations.RemoveField(
            model_name='medication',
            name='date_started',
        ),
        migrations.RemoveField(
            model_name='medication',
            name='drug_instance',
        ),
        migrations.RemoveField(
            model_name='medication',
            name='due',
        ),
        migrations.RemoveField(
            model_name='medication',
            name='pro_re_nata',
        ),
        migrations.RemoveField(
            model_name='medication',
            name='stock',
        ),
        migrations.AddField(
            model_name='drug',
            name='brand_name',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='drug',
            name='dosage',
            field=models.CharField(blank=True, max_length=70),
        ),
        migrations.AddField(
            model_name='medication',
            name='drug',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='medication.drug', verbose_name='Medication'),
        ),
        migrations.DeleteModel(
            name='Brand',
        ),
        migrations.DeleteModel(
            name='Category',
        ),
        migrations.DeleteModel(
            name='DrugInstance',
        ),
    ]
