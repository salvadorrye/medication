# Generated by Django 3.1.6 on 2021-04-17 06:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medication', '0006_auto_20210417_1420'),
    ]

    operations = [
        migrations.RenameField(
            model_name='drug',
            old_name='target_organ_system',
            new_name='subcategory',
        ),
    ]
