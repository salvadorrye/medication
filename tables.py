import django_tables2 as tables
from django_tables2.utils import A
from .models import Medication

class MedicationTable(tables.Table):
    change = tables.LinkColumn('geriatrics:medication:meds-update', args=[A('pk')],
            attrs={'a': {'class': 'update-meds btn btn-warning btn-sm'}}, text='Edit')
    remove = tables.LinkColumn('geriatrics:medication:meds-delete', args=[A('pk')],
            attrs={'a': {'class': 'delete-meds btn btn-danger btn-sm'}}, text='Delete')

    class Meta:
        model = Medication
        template_name = 'core/bootstrap.html'
        exclude = ('id',
                   'created',
                   'last_modified',
                   'admission',
                   'discontinued'
                   )

