from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import Drug, Medication, TimeToBeGiven, \
        MedicationSheet, MedicationGiven

class MedicationAdminSite(AdminSite):
    site_header = 'JHWC Medication Admin'
    site_title = 'JHWC Medication Admin Portal'
    index_title = 'Welcome to JHWC Medication Portal'

medication_admin_site = MedicationAdminSite(name='medication_admin')

@admin.register(Drug)
class DrugAdmin(admin.ModelAdmin):
    search_fields = ['generic_name', 'brand_name']
    list_display = ['generic_name', 'brand_name', 'dosage']

class TimeToBeGivenInline(admin.TabularInline):
    model = TimeToBeGiven

@admin.register(Medication)
class MedicationAdmin(admin.ModelAdmin):
    list_display = ['admission', 'date_ordered', 'drug', 'frequency', 'route', 'time_given_display', 'discontinued']
    list_filter = ['admission', 'date_ordered', 'discontinued']
    search_fields = ['drug__generic_name', 'drug__brand_name']
    raw_id_fields = ['drug']
    inlines = [TimeToBeGivenInline]

    def time_given_display(self, obj):
        return ", ".join([
            tg.time_given.strftime('%I:%M:%p') for tg in obj.order.all()
        ])
    time_given_display.short_description = "Times given"

class MedicationGivenInline(admin.TabularInline):
    model = MedicationGiven
    #raw_id_fields = ['medication']

@admin.register(MedicationSheet)
class MedicationSheetAdmin(admin.ModelAdmin):
    list_display = ['date', 'shift', 'nurse']
    list_filter = ['date']
    inlines = [MedicationGivenInline]

    def save_model(self, request, obj, form, change):
        obj.nurse = request.user
        super().save_model(request, obj, form, change)

medication_admin_site.register(Drug, admin_class=DrugAdmin)
medication_admin_site.register(Medication, admin_class=MedicationAdmin)
medication_admin_site.register(MedicationSheet, admin_class=MedicationSheetAdmin)

