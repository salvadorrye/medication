from django.views.generic import ListView
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Medication
from .forms import MedicationCreateForm
from geriatrics.models import Admission

from .tables import MedicationTable

from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView, BSModalDeleteView

class MedicationsList(LoginRequiredMixin, ListView):
    model = Medication
    template_name = 'medication/includes/medications.html'

    def get_context_data(self, *args, **kwargs):
        request = self.request
        context = super(MedicationsList, self).get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                data = Medication.objects.filter(admission=admission)
                if data:
                    meds_table = MedicationTable(data)
                    meds_table.paginate(page=request.GET.get("page", 1), per_page=5)
                    context['table'] = meds_table
                else:
                    context['table'] = None
            except Admission.DoesNotExist:
                pass
        return context

class MedicationCreate(LoginRequiredMixin, BSModalCreateView):
    model = Medication 
    form_class = MedicationCreateForm
    success_message = 'Medication order was created.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_form(self, *args, **kwargs):
        form = super(MedicationCreate, self).get_form(*args, **kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                form.fields['admission'].initial = admission
                self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
            except Admission.DoesNotExist:
                pass
        return form

    def get_context_data(self, **kwargs):
        context = super(MedicationCreate, self).get_context_data(**kwargs)
        context['title'] = 'Create Medication'
        context['btn'] = 'Create'
        return context

class MedicationUpdate(LoginRequiredMixin, BSModalUpdateView):
    model = Medication
    form_class = MedicationCreateForm
    template_name = 'medication/medication_form.html'
    success_message = 'Medication order was updated.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        meds = super(MedicationUpdate, self).get_object(*args, **kwargs)
        admission = meds.admission
        #added_by = self.request.user
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return meds 

    def get_context_data(self, **kwargs):
        context = super(MedicationUpdate, self).get_context_data(**kwargs)
        context['title'] = 'Update Medication'
        context['btn'] = 'Update'
        return context

class MedicationDelete(LoginRequiredMixin, BSModalDeleteView):
    model = Medication
    success_message = 'Medication oder deleted.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        meds = super(MedicationDelete, self).get_object(*args, **kwargs)
        admission = meds.admission
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return meds 
